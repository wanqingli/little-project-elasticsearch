package com.ruyuan.little.project.elasticsearch.biz.common.constant;

/**
 * @author dulante
 * version: 1.0
 * Description:es 查询字段名称常量
 **/
public class QueryFiledNameConstant {

    /**
     * keyword
     */
    public static final String KEYWORD = "keyword";

    /**
     * 店铺名称
     */
    public static final String STORE_NAME = "storeName";

    /**
     * 商品名称
     */
    public static final String GOODS_NAME = "goodsName";

    /**
     * _id字段
     */
    public static final String UNDERSCORE_ID = "_id";

    /**
     * 上线时间
     */
    public static final String ONLINE_TIME = "onlineTime";

    /**
     * 商品状态
     */
    public static final String GOODS_STATUS = "goodsStatus";

    /**
     * 商品搜索建议名称
     */
    public static final String GOODS_SUGGEST_NAME = "goodsSuggest";

    /**
     * 搜索建议字段
     */
    public static final String SUGGEST = "suggest";

    /**
     * 商品价格字段
     */
    public static final String GOODS_PRICE = "goodsPrice";

    /**
     * 商品编号分组名称
     */
    public static final String GROUP_GOODS_SPU_NO = "groupByGoodsSpuNo";

    /**
     * 商品编号字段
     */
    public static final String GOODS_SPU_NO = "goodsSpuNo";

    /**
     * 商品sku分组名称
     */
    public static final String GROUP_GOODS_SKU = "groupByGoodsSku";

    /**
     * 商品总销量名称
     */
    public static final String SUM_GOODS_SALE_NUM = "sumGoodsSaleNum";

    /**
     * 商品销量字段
     */
    public static final String GOODS_SALE_NUM = "goodsSaleNum";

    /**
     * 手机号字段
     */
    public static final String PHONE_NUMBER = "phoneNumber";

    /**
     * 状态字段
     */
    public static final String STATUS = "status";

    /**
     * 创建时间字段
     */
    public static final String CREATE_TIME = "createTime";
}