package com.ruyuan.little.project.elasticsearch.biz.api.dto;

import lombok.Data;

/**
 * @author <a href="mailto:little@163.com">little</a>
 * version: 1.0
 * Description:订单查询请求信息
 **/
@Data
public class OrderSearchDTO {

    /**
     * 查询内容
     */
    private String searchContent;

    /**
     * 手机号
     */
    private String phoneNumber;

    /**
     * 订单状态
     * {@link com.ruyuan.little.project.elasticsearch.biz.api.enums.OrderStatusEnum}
     */
    private String orderStatus;

    /**
     * 页码
     */
    private Integer page;

    /**
     * 每页大小
     */
    private Integer size;

}
