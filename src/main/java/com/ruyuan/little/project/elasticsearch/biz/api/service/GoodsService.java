package com.ruyuan.little.project.elasticsearch.biz.api.service;

import com.ruyuan.little.project.common.dto.CommonResponse;
import com.ruyuan.little.project.elasticsearch.biz.api.dto.GoodsSearchDTO;
import com.ruyuan.little.project.elasticsearch.biz.api.entity.Order;

/**
 * @author <a href="mailto:little@163.com">little</a>
 * version: 1.0
 * Description:elasticsearch实战
 **/
public interface GoodsService {

    /**
     * 通过wildcard查询商品spu推荐建议
     *
     * @param context 搜索内容
     * @return 结果
     */
    CommonResponse searchSpuSuggestWithWildcard(String context);

    /**
     * 查询商品spu推荐建议
     *
     * @param context 搜索内容
     * @return 结果
     */
    CommonResponse searchSpuSuggest(String context);

    /**
     * 分页查询商品列表
     *
     * @param goodsSearchDTO 商品查询请求信息
     * @return 结果
     */
    CommonResponse getSpuList(GoodsSearchDTO goodsSearchDTO);

    /**
     * 排序分页查询商品列表
     *
     * @param goodsSearchDTO 商品查询请求信息
     * @return 结果
     */
    CommonResponse getOrderSpuList(GoodsSearchDTO goodsSearchDTO);

    /**
     * 根据id查询商品Spu详情
     *
     * @param id 商品spuId
     * @return 结果
     */
    CommonResponse getSpuDetailById(String id);

    /**
     * 根据店铺id获取店铺信息
     *
     * @param id   店铺id
     * @param page 页码
     * @param size 每页大小
     * @return 结果
     */
    CommonResponse getStoreById(String id, Integer page, Integer size);

    /**
     * 扣减库存
     *
     * @param order 订单信息
     * @return 结果
     */
    CommonResponse deductStock(Order order);

    /**
     * 还原库存
     *
     * @param order 订单信息
     * @return 结果
     */
    CommonResponse unDeductStock(Order order);

}
